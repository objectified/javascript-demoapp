pipeline {
    agent any

    tools {
        nodejs 'Default NodeJS'
    }

    stages {
        stage('Build') {
            steps {
                git url: 'https://bitbucket.org/objectified/javascript-demoapp'

                sh 'npm install'
            }
        }

        stage('Static Security Checks') {
            steps {
                sh './node_modules/.bin/tslint --force --format pmd -o tslint-security-pmd.xml --project tsconfig.json --config tslint.json'
            }
        }

	stage('Detect Secrets') {

	    steps {
                script {
                    sh './run-detect-secrets.sh'
                }
            }
	}

        stage('Security Check: NPM Dependencies') {
            steps {
                dependencyCheck additionalArguments: '', odcInstallation: 'My Dependency-Check'
            }
        }

        stage('Security Check: Dockerfile') {
            steps {
                sh '/hadolint --format checkstyle Dockerfile 2>&1 | tee hadolint-checkstyle-result.xml'
            }
        }

        stage('Publish Docker Image') {
            steps {
                script {
                    docker.build "demoapp-js"
                }
            }
        }

        stage('Security Check: Scan Docker Image') {
            steps {
                script {
                    sh './run-trivy.sh'
                }
            }
        }

        stage('Start DAST job') {
            steps {
                build job: 'demoapp-js-dast', wait: false
            }
        }
    }

    post {
        always {
            dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
            archiveArtifacts artifacts: '**/trivy-report.html'
            archiveArtifacts artifacts: '**/secrets.html'
            recordIssues tool: spotBugs(name: 'Code Security')
            recordIssues tool: checkStyle(pattern: '**/hadolint-checkstyle-result.xml', name: 'Dockerfile Security')
            recordIssues tool: pmdParser(pattern: '**/tslint-security-pmd.xml', name: 'ESLint Angular Security')
	    publishHTML (target: [
	    	allowMissing: false,
		alwaysLinkToLastBuild: false,
		keepAll: true,
		reportDir: '',
		reportFiles: 'trivy-report.html',
		reportName: "Docker Container Scan"
	    ])

	    publishHTML (target: [
	    	allowMissing: false,
		alwaysLinkToLastBuild: false,
		keepAll: true,
		reportDir: '',
		reportFiles: 'secrets.html',
		reportName: "Secrets Scan"
	    ])
        }
    }

}
